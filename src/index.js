import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const products = [
	{ id: 'm1', name: 'Apple iPhone 6', image: '/images/iphone6.png', brand: 'Apple', dimension: '123.88 x 58.6 x 7.6', network: 'HSPA (3.5G)', os: 'Android 7.0 Nougat', standby_time: 'Up to 313 hrs (2G/3G/LTE)', talk_time: 'Up to 10 hrs (2G/3G/LTE)', weight: '145g', display: '2880 x 1440 pixels', internal_memory: '64 GB', external_memory: '2 TB'},
	{ id: 'm2', name: 'Asus Zenfone', image: '/images/asus_zenfone.png', brand: 'Apple', dimension: '123.88 x 58.6 x 7.6', network: 'HSPA (3.5G)', os: 'Android 7.0 Nougat', standby_time: 'Up to 313 hrs (2G/3G/LTE)', talk_time: 'Up to 10 hrs (2G/3G/LTE)', weight: '145g', display: '2880 x 1440 pixels', internal_memory: '64 GB', external_memory: '2 TB'},
	{ id: 'm3', name: 'Samsung S4', image: '/images/samsungS4.png', brand: 'Apple', dimension: '123.88 x 58.6 x 7.6', network: 'HSPA (3.5G)', os: 'Android 7.0 Nougat', standby_time: 'Up to 313 hrs (2G/3G/LTE)', talk_time: 'Up to 10 hrs (2G/3G/LTE)', weight: '145g', display: '2880 x 1440 pixels', internal_memory: '64 GB', external_memory: '2 TB'}
];

function Product(props) {
  const details = props.items.map((product) =>
    <div>
        <h1>
          {product.name}
        </h1>
        <p>
          <ul>
            <li>
              Brand : {product.brand}
            </li>
            <li>
              Dimension : {product.dimension}
            </li>
            <li>
              Network : {product.network}
            </li>
          </ul>
        </p>
    </div>
  );

  return (
    <div>
      {details}
    </div>
  );
};

ReactDOM.render(
  <Product items={products} />,
  document.getElementById('root')
);